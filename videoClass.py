import subprocess, os, shutil
from time import time



#Get png dir
#Get parameters, fps, bitrate
#Create video in Videos dir. Take name from dir
#Try to create from in memory images video creation


class VideoMaker:
    def __init__(self,rootDir):
        self.rootDir = rootDir
        self.videoDir = "{}/simOutput/Videos".format(self.rootDir)
        if not (os.path.isdir(self.videoDir)):
            os.mkdir(self.videoDir)

    def getVideoPath(self,imageDir):
        videoFileName = os.path.basename(imageDir)
        videoPath = "{}/{}.mp4".format(self.videoDir, videoFileName)
        return videoPath

    def getAllPngDirs(self):
        contentDir = "{}/simOutput".format(self.rootDir)
        rootContent = [os.path.join(contentDir,path) for path in os.listdir(contentDir)]
        rootDirs = [path for path in rootContent if os.path.isdir(path)]
        rootPngDirs = [path for path in rootDirs if ("png" in os.path.basename(path))]
        return rootPngDirs

    def createVideo(self, imageDir, videoPath, fps = 20, encoder="h264_nvenc", bitrate = "2M"):
        timeStart = time()
        createVideoCommand = "ffmpeg -y -loglevel quiet -framerate {0} -pattern_type glob -i '{3}/*.png' -pix_fmt yuv420p -b:v {2} -c:v {1} {4}"
        createVideo = createVideoCommand.format(fps, encoder, bitrate, imageDir, videoPath)
        videoProcess = subprocess.run(createVideo, shell=True, 
            universal_newlines=True, start_new_session=True)
        print("Video {} has been generated in {:.2f} seconds".format(os.path.basename(videoPath), time()-timeStart))

    def deleteImages(self):
        imageDirArray = self.getAllPngDirs()
        #Delete images after generation
        for imageDir in imageDirArray:
            shutil.rmtree(imageDir)

    def createAllVideos(self, **kwargs):
        imageDirArray = self.getAllPngDirs()
        videoFiles = kwargs.pop("videos")
        if (videoFiles!="all"):
            videoFiles = videoFiles.split(",")
            imageDirArray = [imagePath for dirName in videoFiles for imagePath in imageDirArray if (dirName in imagePath)]
        for imageDir in imageDirArray:
            videoPath = self.getVideoPath(imageDir)
            self.createVideo(imageDir, videoPath, **kwargs)
        self.deleteImages()
        print("All videos has been generated. Images deleted")



if (__name__ == "__main__"):
    videoMaker = VideoMaker("/home/bohdan/PIConGPU/projects/PlasmaEnlightenment/runs/lwfa_001/3")
    videoMaker.createAllVideos(videos = "pngUnified", encoder = "hevc_nvenc", bitrate="10M")