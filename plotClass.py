import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
from PIL import Image

from sympy import sympify
from scipy.constants import pi,e,m_e,c
import json, os, math
import numpy as np



#Пренебрежем оптимизацией
#Нужно быстро - есть PIL
#А графики генерируются долго
class PlotGenerator:
    @staticmethod
    def createAndSavePlot(plotParameters, imagePath, electron, ion, E, B):
        def setSubplot(ax, value, title, maxIndex):
            #На видео хорошо видно, что картинка ЧБ адабтируется под max, это сбивает всю физику
            #Сравнить яркости с PIL, как там это происходит
            ax.imshow(value, **paramDict)
            ax.set_title(title, fontsize=30)
            ax.axis('off')
            cbar = fig.colorbar(ax=ax,ticks =[0,maxIndex/2,maxIndex],format= '%.2f',
                mappable = mpl.cm.ScalarMappable(norm = mpl.colors.Normalize(vmin=0, vmax=maxIndex), cmap='gray'))
            cbar.ax.tick_params(labelsize=25) 

        ((Etitle, Emax), (Btitle, Bmax), (elTitle, elMax), (ionTitle, ionMax)) = plotParameters
        paramDict = {"cmap":'gray',
                     #Система сама хорошо выбирает интерполяцию
                     "vmin":0, "vmax":255}
        fig, axs = plt.subplots(2, 2, figsize = np.array([1434, 1076])/100)
    
        setSubplot(axs[0,0], E, Etitle, Emax)
        setSubplot(axs[0,1], B, Btitle, Bmax)
        setSubplot(axs[1,0], electron, elTitle, elMax)
        setSubplot(axs[1,1], ion, ionTitle, ionMax)
        plt.tight_layout()
        fig.savefig(imagePath)
        plt.close(fig)

    @staticmethod
    def checkParametersPresence(parameters):
        neededParameters = ["BASE_DENSITY_SI","PPC","WAVE_LENGTH_SI",
                            "PARAM_A0","DELTA_T_SI","CELL_WIDTH_SI"]
        for paramName in neededParameters:
            if (paramName not in parameters):
                raise ValueError("{} is needed but not present in parameters.json".format(paramName))

    @staticmethod
    def getParameterByIndex(valueArray,index):
        value = None
        while (value is None):
            if (type(valueArray) is list) and (valueArray[index] != "//"):
                value = valueArray[index]
            elif (type(valueArray) is dict) and (str(index+1) in valueArray):
                value = valueArray[str(index+1)]
            elif (type(valueArray) is str) and (index == 0):
                value = valueArray
            index-=1
        return value
    
    @staticmethod
    def getAllParameters(parameters, rootDir):
        index = int(os.path.basename(rootDir))-1
        parametersIndexed = {}
        for paramName, valueArray in parameters.items():
            value = PlotGenerator.getParameterByIndex(valueArray, index)
            parametersIndexed[paramName]=value
        return parametersIndexed

    @staticmethod
    def renameParameters(parameters):
        parametersNamed = {
            "density": parameters["BASE_DENSITY_SI"],
            "ppc": parameters["PPC"],
            "Lambda":parameters["WAVE_LENGTH_SI"],
            "A0":parameters["PARAM_A0"],
            "dt":parameters["DELTA_T_SI"],
            "dx":parameters["CELL_WIDTH_SI"],
        }
        for paramName in parametersNamed:
            parametersNamed[paramName] = float(sympify(parametersNamed[paramName]))
        return parametersNamed

    @staticmethod
    def readParametersJson(rootDir):
        #Density, PPC,      dx,dt       A0,w0
        jsonPath = os.path.realpath(os.path.join(rootDir,"../parameters.json"))
        with open(jsonPath) as jsonFile:
            parametersJson = json.loads(jsonFile.read())
            parameters = parametersJson["parameters"]
        PlotGenerator.checkParametersPresence(parameters)
        parametersIndexed = PlotGenerator.getAllParameters(parameters, rootDir)
        parametersNamed = PlotGenerator.renameParameters(parametersIndexed)
        return parametersNamed

    @staticmethod
    def generateNameAndMaxIndex(rootDir):
        def getOrder(value):
            order = math.floor(math.log10(value))
            return order
        # Only if dx=dy=dz. В расчетах маштаба больше всего риск ошибки
        # Куча констант, которые нужно подставлять руками
        parameters = PlotGenerator.readParametersJson(rootDir)
        parameters['ppm'] = parameters['density']*parameters['dx']**3/parameters['ppc']
        Emax = parameters['A0']*(2*pi*m_e*c**2)/(e*parameters['Lambda'])
        Bmax = Emax / c**2
        elMax = parameters['density']
        ionMax = parameters['density']

        Etitle = '$|E_y|, 10^{%d} V/m$' % (getOrder(Emax))
        Btitle = r'$|B_z|, 10^{%d} T$' % (getOrder(Bmax))
        elTitle = r'$N_e, 10^{%d} m^{{-3}}$' % (getOrder(elMax))
        ionTitle = r'$N_i, 10^{%d} m^{{-3}}$' % (getOrder(ionMax))
        
        Emax, Bmax, elMax, ionMax = [value/10**getOrder(value) for value in [Emax, Bmax, elMax, ionMax]]
        return [[Etitle, Emax], [Btitle, Bmax], [elTitle, elMax], [ionTitle, ionMax]]

    @staticmethod
    def PIL(imagePath, electron, ion, E, B):
        image = ion
        unifiedImage = Image.new('RGB', (2*image.size[0],2*image.size[1]), (255,255,255))
        unifiedImage.paste(E, (0,0))
        unifiedImage.paste(B, (image.size[0],0))
        unifiedImage.paste(electron, (0,image.size[1]))
        unifiedImage.paste(ion, (image.size[0],image.size[1]))
        unifiedImage.save(imagePath)