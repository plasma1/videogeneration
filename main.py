import argparse

from videoClass import VideoMaker
from imageClass import ImageGenerator

#parse parameters. Dir to run processing
#Run full video generation


#Parse arguments
parser = argparse.ArgumentParser()
parser.add_argument("path", help="path to plasma directory")
parser.add_argument("-f","--fps", type=int, default=20, help="fps of generated video")
parser.add_argument("-c","--encoder", default="h264_nvenc", help="encoder used for video generation")
parser.add_argument("-b","--bitrate", default="2M", help="bitrate of generated video")
parser.add_argument("-v","--videos", default="all", help="select what video files to generate")
parser.add_argument("-i","--images", default="pil", help="select what image processing to use")
args = parser.parse_args()


#Generate images and videos

imageGenerator = ImageGenerator(args.path, args.images)
imageGenerator.convertImages()
videoMaker = VideoMaker(args.path)
videoMaker.createAllVideos(fps=args.fps ,encoder = args.encoder, bitrate=args.bitrate, videos=args.videos)