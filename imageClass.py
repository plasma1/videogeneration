import multiprocessing, os
from time import time
from PIL import Image
from plotClass import PlotGenerator


#Split image to E, B, e, i
#By colour
#Add white letter for identification
#Parallel map transform
#Unify 4 GS images to one with coloring

#Check if image size is equal
#Test on one image



class ImageGenerator:
    def __init__(self, rootDir, imgGenerator):
        self.processPool = multiprocessing.Pool(multiprocessing.cpu_count()-2)
        #Уязвимость по статическим именам!
        self.rootDir = rootDir
        self.imgGenerator = imgGenerator
        self.particlePath = rootDir+"/simOutput/pngUnified"
        self.electronsPath = rootDir+"/simOutput/pngElectronsYX"
        self.ionsPath = rootDir+"/simOutput/pngIonsYX"
        if not (os.path.isdir(self.particlePath)):
            os.mkdir(self.particlePath)

    @staticmethod
    def checkEqualSize(imageArray):
        imgSize = imageArray[0].size
        for image in imageArray:
            if (image.size != imgSize):
                return False
        return True

    @staticmethod
    def splitImage(imagePath):
        plasmaImage = Image.open(imagePath)
        particle, E, B = plasmaImage.split()
        return particle, E, B

    @staticmethod
    def createUnifiedImage(mapParameters):
        plotParameters, particlesParams, imgGenerator = mapParameters
        pngElectron, pngIon, particlePath = particlesParams
        electron, E, B = ImageGenerator.splitImage(pngElectron)
        ion, _, _ = ImageGenerator.splitImage(pngIon)

        imageName = "p"+os.path.basename(pngElectron)[1:]
        imagePath = "{}/{}.png".format(particlePath,imageName)

        if (ImageGenerator.checkEqualSize([electron, ion, E, B])):
            if (imgGenerator == "mpl"):
                PlotGenerator.createAndSavePlot(plotParameters,imagePath, electron, ion, E, B)
            elif (imgGenerator == "pil"):
                PlotGenerator.PIL(imagePath, electron, ion, E, B)
        else:
            raise ValueError("Image sizes not equal")


    def convertImages(self):
        print("Started images generation")
        timeStart= time()
        electronImages = [self.electronsPath+"/"+ePath for ePath in sorted(os.listdir(self.electronsPath))]
        ionImages = [self.ionsPath+"/"+iPath for iPath in sorted(os.listdir(self.ionsPath))]
        particlePaths = [self.particlePath]*len(ionImages)
        particleImages = list(zip(electronImages,ionImages, particlePaths))

        if (self.imgGenerator == 'mpl'):
            plotParameters = PlotGenerator.generateNameAndMaxIndex(self.rootDir)
        elif (self.imgGenerator == 'pil'):
            plotParameters = None
        mapParameters = [[plotParameters,image, self.imgGenerator] for image in particleImages]
        self.processPool.map(self.createUnifiedImage,mapParameters)
        self.checkEndOfProcess()
        print("Images have been generated in {:.2f} seconds".format(time()-timeStart))

    def checkEndOfProcess(self):
        self.processPool.close()
        self.processPool.join()
        electronSize = len(os.listdir(self.electronsPath))
        ionSize = len(os.listdir(self.ionsPath))
        if not (electronSize == ionSize):
            raise ValueError("Image generation not ended")




if (__name__ == "__main__"):
    imageGenerator = ImageGenerator("/home/bohdan/PIConGPU/projects/PlasmaEnlightenment/runs/lwfa_002/3","pil")
    imageGenerator.convertImages()